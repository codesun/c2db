module c2db_postgresql.proto;

import std.string;
import core.stdc.config;

/**
 * PostgreSQL raw API for DDBC, obtained from libpq-fe.h
 *
 * @author: qyl
 */
extern(C) {
    /* ========================================== */
    /* | Libpostgresqlclient Typedef Definition | */
    /* ========================================== */
    alias cstring = const(char)*;
    // Object ID is a fundamental type in Postgres.
    alias Oid = uint;
    /* Define to the name of a signed 64-bit integer type. */
    alias PG_INT64_TYPE = long;
    /* Define a signed 64-bit integer type for use in client API declarations.*/
    alias pg_int64 = PG_INT64_TYPE;

    alias PGconn = pg_conn;
    alias PGresult = pg_result;
    alias PGcancel = pg_cancel;

    /* =========================================== */
    /* | Libpostsqlclient Enumeration Definition | */
    /* =========================================== */
    enum Oid InvalidOid = cast(Oid)0;
    enum Oid OID_MAX = uint.max;

    /* definition of the system "type" relation (pg_type) */
    enum : Oid {
        BOOLOID = 16,
        BYTEAOID = 17,
        CHAROID = 18,
        INT8OID = 20,
        INT2OID = 21,
        INT4OID = 23,
        TEXTOID = 25,
        FLOAT4OID = 700,
        FLOAT8OID = 701,
        BPCHAROID = 1042,
        VARCHAROID = 1043
    }
    /*
     * Identifiers of error message fields.  Kept here to keep common
     * between frontend and backend, and also to export them to libpq
     * applications.
     */
	enum : char {
		PG_DIAG_SEVERITY = 'S',
    	PG_DIAG_SQLSTA = 'C',
    	PG_DIAG_MESSAGE_PRIMARY = 'M',
    	PG_DIAG_MESSAGE_DETAIL = 'D',
    	PG_DIAG_MESSAGE_HINT = 'H',
    	PG_DIAG_STATEMENT_POSITION = 'P',
    	PG_DIAG_INTERNAL_POSITION = 'p',
    	PG_DIAG_INTERNAL_QUERY = 'q',
    	PG_DIAG_CONTEXT = 'W',
    	PG_DIAG_SCHEMA_NAME = 's',
    	PG_DIAG_TABLE_NAME = 't',
    	PG_DIAG_COLUMN_NAME = 'c',
    	PG_DIAG_DATATYPE_NAME = 'd',
    	PG_DIAG_CONSTRAINT_NAME = 'n',
    	PG_DIAG_SOURCE_FILE = 'F',
    	PG_DIAG_SOURCE_LINE = 'L',
    	PG_DIAG_SOURCE_FUNCTION = 'R'
	}

    /*
     * Option flags for PQcopyResult
     */
	enum : ubyte {
		PG_COPYRES_ATTRS = 0x01,
    	PG_COPYRES_TUPLES = 0x02,      /* Implies PG_COPYRES_ATTRS */
    	PG_COPYRES_EVENTS = 0x04,
    	PG_COPYRES_NOTICEHOOKS = 0x08
	}

    /* Application-visible enum types */

    /*
     * Although it is okay to add to these lists, values which become unused
     * should never be removed, nor should constants be redefined - that would
     * break compatibility with existing code.
     */

    enum ConnStatusType: uint {
        CONNECTION_OK,
        CONNECTION_BAD,
        /* Non-blocking mode only below here */

        /*
         * The existence of these should never be relied upon - they should only
         * be used for user feedback or similar purposes.
         */
        CONNECTION_STARTED,         /* Waiting for connection to be made.  */
        CONNECTION_MADE,            /* Connection OK; waiting to send.     */
        CONNECTION_AWAITING_RESPONSE,       /* Waiting for a response from the
                                             * postmaster.        */
        CONNECTION_AUTH_OK,         /* Received authentication; waiting for
                                     * backend startup. */
        CONNECTION_SETENV,          /* Negotiating environment. */
        CONNECTION_SSL_STARTUP,     /* Negotiating SSL. */
        CONNECTION_NEEDED           /* Internal state: connect() needed */
    }

    enum ExecStatusType: uint {
        PGRES_EMPTY_QUERY = 0,      /* empty query string was executed */
        PGRES_COMMAND_OK,           /* a query command that doesn't return
                                     * anything was executed properly by the
                                     * backend */
        PGRES_TUPLES_OK,            /* a query command that returns tuples was
                                     * executed properly by the backend, PGresult
                                     * contains the result tuples */
        PGRES_COPY_OUT,             /* Copy Out data transfer in progress */
        PGRES_COPY_IN,              /* Copy In data transfer in progress */
        PGRES_BAD_RESPONSE,         /* an unexpected response was recv'd from the
                                     * backend */
        PGRES_NONFATAL_ERROR,       /* notice or warning message */
        PGRES_FATAL_ERROR,          /* query failed */
        PGRES_COPY_BOTH,            /* Copy In/Out data transfer in progress */
        PGRES_SINGLE_TUPLE          /* single tuple from larger resultset */
    }

    enum PGTransactionStatusType: uint {
        PQTRANS_IDLE,               /* connection idle */
        PQTRANS_ACTIVE,             /* command in progress */
        PQTRANS_INTRANS,            /* idle, within transaction block */
        PQTRANS_INERROR,            /* idle, within failed transaction */
        PQTRANS_UNKNOWN             /* cannot determine status */
    }

    /*
     * PGPing - The ordering of this enum should not be altered because the
     * values are exposed externally via pg_isready.
     */
    enum PGPing: uint {
        PQPING_OK,          /* server is accepting connections */
        PQPING_REJECT,      /* server is alive but rejecting connections */
        PQPING_NO_RESPONSE, /* could not establish connection */
        PQPING_NO_ATTEMPT   /* connection not attempted (bad params) */
    }

    /* ===================== */
    /* | Struct Definition | */
    /* ===================== */

    /* PGconn encapsulates a connection to the backend.
     * The contents of this struct are not supposed to be known to applications.
     */
    struct pg_conn;
    /* PGresult encapsulates the result of a query (or more precisely, of a single
     * SQL command --- a query string given to PQsendQuery can contain multiple
     * commands and thus return multiple PGresult objects).
     * The contents of this struct are not supposed to be known to applications.
     */
    struct pg_result;
    /* PGcancel encapsulates the information needed to cancel a running
     * query on an existing connection.
     * The contents of this struct are not supposed to be known to applications.
     */
    struct pg_cancel;
    /* ----------------
     * PGresAttDesc -- Data about a single attribute (column) of a query result
     * ----------------
     */
    struct PGresAttDesc {
        cstring name;               /* column name */
        Oid tableid;                /* source table, if known */
        int columnid;               /* source column, if known */
        int format;                 /* format code for value (text/binary) */
        Oid typid;                  /* type id */
        int typlen;                 /* type size */
        int atttypmod;              /* type-specific modifier info */
    }
    struct PQconninfoOption;

    /* ======================================== */
    /* | Libpostsqlclient Function Definition | */
    /* ======================================== */

    /* ----------------
     * Exported functions of libpq
     * ----------------
     */
    /* make a new client connection to the backend */
    /* Synchronous (blocking) */
    PGconn* PQconnectdb(cstring conninfo);
    PGconn* PQsetdbLogin(cstring pghost,
                    cstring pgport, cstring pgoptions,
                    cstring pgtty,
                    cstring dbName, cstring login, cstring pwd);

    /* close the current connection and free the PGconn data structure */
    void PQfinish(PGconn* conn);

    /*
     * close the current connection and restablish a new one with the same
     * parameters
     */
    /* Synchronous (blocking) */
    void PQreset(PGconn* conn);

    /* Accessor functions for PGconn objects */
    char* PQoptions(const(PGconn)* conn);
    ConnStatusType PQstatus(const(PGconn)* conn);
    PGTransactionStatusType PQtransactionStatus(const(PGconn)* conn);
    cstring PQparameterStatus(const(PGconn)* conn, cstring paramName);
    int PQprotocolVersion(const(PGconn)* conn);
    int PQserverVersion(const(PGconn)* conn);
    char* PQerrorMessage(const(PGconn)* conn);

    /* === in fe-exec.c === */

    /* Simple synchronous query */
    PGresult* PQexec(PGconn* conn, cstring query);
    PGresult* PQexecParams(PGconn* conn,
                        cstring command,
                        int nParams,
                        const(Oid)* paramTypes,
                        const(cstring)* paramValues,
                        const(int)* paramLengths,
                        const(int)* paramFormats,
                        int resultFormat);

    /* Interface for multiple-result or asynchronous queries */
    int PQsendQuery(PGconn* conn, cstring query);
    int PQsetSingleRowMode(PGconn* conn);
    PGresult* PQgetResult(PGconn* conn);

    /* Accessor functions for PGresult objects */
    ExecStatusType PQresultStatus(const(PGresult)* res);
    char* PQresStatus(ExecStatusType status);
    char* PQresultErrorMessage(const(PGresult)* res);
    char* PQresultErrorField(const(PGresult)* res, int fieldcode);
    int PQntuples(const(PGresult)* res);
    int PQnfields(const(PGresult)* res);
    cstring PQfname(const(PGresult)* res, int field_num);
    int PQfnumber(const(PGresult)* res, cstring field_name);
    Oid PQftable(const(PGresult)* res, int field_num);
    int PQftablecol(const(PGresult)* res, int field_num);
    int PQfformat(const(PGresult)* res, int field_num);
    Oid PQftype(const(PGresult)* res, int field_num);
    int PQfsize(const(PGresult)* res, int field_num);
    int PQfmod(const(PGresult)* res, int field_num);
    Oid PQoidValue(const(PGresult)* res);
    char* PQcmdTuples(PGresult* res);
    char* PQgetvalue(const(PGresult)* res, int tup_num, int field_num);
    int PQgetlength(const(PGresult)* res, int tup_num, int field_num);
    int PQgetisnull(const(PGresult)* res, int tup_num, int field_num);

    /* Delete a PGresult */
    void PQclear(PGresult* res);

    /* Create and manipulate PGresults */
    PGresult* PQmakeEmptyPGresult(PGconn* conn, ExecStatusType status);
    int PQsetResultAttrs(PGresult* res, int numAttributes, PGresAttDesc* attDescs);
    int PQsetvalue(PGresult* res, int tup_num, int field_num, char* value, int len);

    /* Quoting strings before inclusion in queries. */
    size_t PQescapeStringConn(PGconn* conn,
                            char* to, cstring from, size_t length,
                            int* error);
    ubyte* PQescapeLiteral(PGconn* conn, cstring str, size_t len);

    /* Get the version of the libpq library in use */
    int PQlibVersion();
}
