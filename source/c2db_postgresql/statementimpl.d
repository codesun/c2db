module c2db_postgresql.statementimpl;

import c2db.statement;
import c2db.resultset;
import c2db.connection;
import c2db_postgresql.resultsetimpl;
import c2db_postgresql.exception;
import c2db_postgresql.connectionimpl;
import c2db_postgresql.proto;

import std.string;
import std.conv;
import core.stdc.config;

class StatementImpl : Statement {
private:
    PGresult*[] results;

	Connection pconn;

	string batch;

	PGconn* conn;

    ulong affectedRows;
	uint createdResultSet;
    uint index;

	bool enableEscape_;
	bool isClosed_;
    bool haveUpdateOperation_;


	string escapeSql(string sql, string file, size_t line) {
		// it is safest way to contain the encoded sql
		char[] str = new char[2 * sql.length];
		scope(exit) {
			destroy(str);
		}
        int* error = null;
		size_t len = PQescapeStringConn(
				conn, str.ptr, sql.ptr, sql.length, error);
        if (*error != 0) {
			string msg = fromStringz(PQerrorMessage(conn)).idup;
			throw new PostgreSQLException(msg, file, line);
        }
		return str[0 .. len].idup;
	}
public:
	this(Connection pconn, PGconn* conn) {
		isClosed_ = false;
		enableEscape_ = false;
        haveUpdateOperation_ = false;
		createdResultSet = 0;
        affectedRows = 0;
        index = 0;

		this.pconn = pconn;
		this.conn = conn;
	}
	void close(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(createdResultSet != 0) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.UNCLOSE, file, line);
		}
		isClosed_ = true;
		ConnectionImpl connImpl = cast(ConnectionImpl)pconn;
		connImpl.releaseStatement();
	}
	Connection getConnection(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		return pconn;
	}
	bool isClosed() {
		return isClosed_;
	}
	void setEscapeProcessing(bool enable, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		enableEscape_ = enable;
	}

	bool execute(string sql, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(enableEscape_) {
			sql = escapeSql(sql, file, line);
		}
		int ret = PQsendQuery(conn, sql.ptr);
		if(ret == 0) {
			string msg = fromStringz(PQerrorMessage(conn)).idup;
			throw new PostgreSQLException(msg, file, line);
		}
        bool error = false;
        bool isQuery = false;
        ulong affected = 0;
        PGresult* result = PQgetResult(conn);
        while (result != null) {
            ExecStatusType status = PQresultStatus(result);
            if(status != ExecStatusType.PGRES_COMMAND_OK
                    && status != ExecStatusType.PGRES_TUPLES_OK) {
                PQclear(result);
                error = true;
            } else {
                if (status == ExecStatusType.PGRES_TUPLES_OK) {
                    results ~= result;
                    isQuery = true;
                } else {
                    haveUpdateOperation_ = true;
                    char[] rows = fromStringz(PQcmdTuples(result));
                    if (rows.length != 0) {
                        affected += to!(ulong)(rows);
                    }
                }
            }
            result = PQgetResult(conn);
        }
        if(error) {
            string msg = fromStringz(PQerrorMessage(conn)).idup;
            throw new PostgreSQLException(msg, file, line);
        }
        affectedRows = affected;
		return isQuery;
	}

	ResultSet executeQuery(string sql, string file, size_t line) {
		bool ret = execute(sql, file, line);
		if(ret != true) {
			throw new PostgreSQLException(EXECUTE_EXCEPTION.QUERY, file, line);
		}
		return getResultSet(file, line);
	}
	ulong executeUpdate(string sql, string file, size_t line) {
		bool ret = execute(sql, file, line);
		if(ret == true) {
			throw new PostgreSQLException(EXECUTE_EXCEPTION.UPDATE, file, line);
		}
		return affectedRows;
	}

	void addBatch(string sql, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(batch == null) {
			batch = sql;
			return;
		}
		batch ~= ";";
		batch ~= sql;
	}
	void clearBatch(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		batch = null;
	}
	void executeBatch(string file, size_t line) {
		execute(batch, file, line);
	}

	bool getMoreResults(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
        if (index != results.length) {
            ++index;
            return true;
        }
        return false;
	}
	ResultSet getResultSet(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
        if (results.length == 0) {
            if(haveUpdateOperation_) {
                throw new PostgreSQLException(RESULT_EXCEPTION.UPDATE,
                                            file, line);
            }
			throw new PostgreSQLException(RESULT_EXCEPTION.NOOPERATION,
                                        file, line);
        }
		++createdResultSet;
		return new ResultSetImpl(this, results[index]);
	}
	// not public for DDBC
	void releaseResultSet() {
		--createdResultSet;
	}
}

enum RESULT_EXCEPTION : string {
	UPDATE = "update operation doesn't have resultset",
    NOOPERATION = "no query or update has been executed"
}
enum CLOSE_EXCEPTION : string {
	UNCLOSE = "statement has resultset unclosed",
	ALCLOSE = "statement already closed"
}
enum EXECUTE_EXCEPTION : string {
	QUERY = "operation doesn't belong to query",
	UPDATE = "operation doesn't belong to update"
}
