module c2db_postgresql.connectionimpl;

import c2db.connection;
import c2db.statement;
import c2db_postgresql.exception;
import c2db_postgresql.proto;
import c2db_postgresql.statementimpl;

import std.string;

class ConnectionImpl : Connection {
private:
	PGconn* conn;
    PGresult* transaction;
	uint createdStatement;

	bool isClosed_;
	bool isAutoCommit_;

public:
	this(PGconn* conn) {
		isClosed_ = false;
		// auto commit mode by default
		isAutoCommit_ = true;
		createdStatement = 0;

		this.conn = conn;
	}
    ~this() {
        PQclear(transaction);
    }
	bool isClosed() {
		return isClosed_;
	}

	bool getAutoCommit(string file = __FILE__, size_t line = __LINE__) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		return isAutoCommit_;
	}

	void close(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(createdStatement != 0) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.UNCLOSE, file, line);
		}
		isClosed_ = true;
        PQclear(transaction);
		PQfinish(conn);
	}

	void commit(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
        transaction = PQexec(conn, "END");
		if(PQresultStatus(transaction) != ExecStatusType.PGRES_COMMAND_OK) {
			string msg = fromStringz(PQresultErrorMessage(transaction)).idup;
            PQclear(transaction);
			throw new PostgreSQLException(msg, file, line);
		}
        PQclear(transaction);
        transaction = PQexec(conn, "BEGIN");
        if(PQresultStatus(transaction) != ExecStatusType.PGRES_COMMAND_OK) {
            string msg = fromStringz(PQresultErrorMessage(transaction)).idup;
            PQclear(transaction);
            throw new PostgreSQLException(msg, file, line);
        }
        PQclear(transaction);
	}

	void rollback(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
        transaction = PQexec(conn, "ROLLBACK");
		if(PQresultStatus(transaction) != ExecStatusType.PGRES_COMMAND_OK) {
			string msg = fromStringz(PQresultErrorMessage(transaction)).idup;
            PQclear(transaction);
			throw new PostgreSQLException(msg, file, line);
		}
        PQclear(transaction);
        transaction = PQexec(conn, "BEGIN");
        if(PQresultStatus(transaction) != ExecStatusType.PGRES_COMMAND_OK) {
            string msg = fromStringz(PQresultErrorMessage(transaction)).idup;
            PQclear(transaction);
            throw new PostgreSQLException(msg, file, line);
        }
        PQclear(transaction);
	}

	void setAutoCommit(bool autoCommit, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(!autoCommit) {
            transaction = PQexec(conn, "BEGIN");
            if(PQresultStatus(transaction) != ExecStatusType.PGRES_COMMAND_OK) {
                string msg = fromStringz(PQresultErrorMessage(transaction)).idup;
                PQclear(transaction);
                throw new PostgreSQLException(msg, file, line);
            }
            PQclear(transaction);
		} else {
            transaction = PQexec(conn, "END");
            PQclear(transaction);
		}
		isAutoCommit_ = autoCommit;
	}
	Statement createStatement(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		++createdStatement;
		return new StatementImpl(this, conn);
	}
	// not public for DDBC
	void releaseStatement() {
		--createdStatement;
	}
}

enum CLOSE_EXCEPTION : string {
	UNCLOSE = "connection has statement unclosed",
	ALCLOSE = "connection already closed"
}
