module c2db_postgresql.init;

import c2db.driver;
import c2db_postgresql.driverimpl;

extern(C) {
	export Driver init() {
		return new DriverImpl();
	}
}

version(Windows) {
	import core.runtime;
	import core.sys.windows.windows;

	HINSTANCE g_hInst;

	extern(Windows)
	BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID pvReserved) {
		switch(ulReason) {
			case DLL_PROCESS_ATTACH:
				Runtime.initialize();
				break;
			case DLL_PROCESS_DETACH:
				Runtime.terminate();
				break;
			case DLL_THREAD_ATTACH:
				break;
			case DLL_THREAD_DETACH:
				break;
			default:
				assert(false);
		}
		g_hInst = hInstance;
		return true;
	}
}
