module c2db_postgresql.exception;

import c2db.exception;

class PostgreSQLException : SQLException {
public:
	this(string msg, string file = __FILE__, size_t line = __LINE__) {
		super(msg, file, line);
	}
}
