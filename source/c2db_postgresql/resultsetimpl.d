module c2db_postgresql.resultsetimpl;

import c2db.resultset;
import c2db.statement;
import c2db_postgresql.exception;
import c2db_postgresql.proto;
import c2db_postgresql.statementimpl;

import std.string;
import std.conv;

class ResultSetImpl : ResultSet {
private:
	uint[string] indexMap;

	Statement stat;

	PGresult* result;

	size_t cursor;
    size_t rowNum;
    uint fileldNum;

	bool isClosed_;

	bool outOfBoundary;


	void prepareFields() {
		fileldNum = cast(uint)PQnfields(result);
		for(uint i = 0; i < fileldNum; ++i) {
			indexMap[fromStringz(PQfname(result, i))] = i;
		}
	}
	void prepareRows() {
		// warning: may cause bug when rows count > uint.max inside x86
		rowNum = to!(size_t)(PQntuples(result));
	}

	T getColumn(T)(size_t columnIndex, string file, size_t line) {
		if(columnIndex >= fileldNum) {
			throw new PostgreSQLException(COLUMN_EXCEPTION.INDEX, file, line);
		}
		string val = fromStringz(PQgetvalue(result,
                                cast(int)cursor, cast(int)columnIndex)).idup;
		return to!T(val);
	}
	uint getColumnIndex(string label, string file, size_t line) {
		if(label !in indexMap) {
			throw new PostgreSQLException(COLUMN_EXCEPTION.LABEL, file, line);
		}
		return indexMap[label];
	}
public:
	this(Statement stat, PGresult* result) {
		isClosed_ = false;
		this.stat = stat;
		this.result = result;
		prepareFields();
		prepareRows();
		outOfBoundary = true;
	}
	Statement getStatement(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		return stat;
	}
	void close(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		isClosed_ = true;
		indexMap = null;
		PQclear(result);
		StatementImpl state = cast(StatementImpl)stat;
		state.releaseResultSet();
	}

	bool isClosed() {
		return isClosed_;
	}

	bool isFirst(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		return !outOfBoundary && cursor == 0;
	}

	bool isLast(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		return !outOfBoundary && cursor == rowNum - 1;
	}

	bool next(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		if(!outOfBoundary) {
			if(cursor == rowNum - 1) {
				outOfBoundary = true;
				return false;
			}
			++cursor;
			return true;
		} else {
			if(cursor == 0) {
				outOfBoundary = false;
				return true;
			}
			return false;
		}
	}
	bool previous(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		if(!outOfBoundary) {
			if(cursor == 0) {
				outOfBoundary = true;
				return false;
			}
			--cursor;
			return true;
		} else {
			if(cursor == rowNum - 1) {
				outOfBoundary = false;
				return true;
			}
			return false;
		}
	}
	bool absolute(size_t rowIndex, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		if(rowIndex < 0 || rowIndex > rowNum - 1) {
			return false;
		}
		cursor = rowIndex;
		outOfBoundary = false;
		return true;
	}
	bool first(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		if(rowNum == 0) {
			return false;
		}
		cursor = 0;
		outOfBoundary = false;
		return true;
	}
	bool last(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		if(rowNum == 0) {
			return false;
		}
		cursor = rowNum - 1;
		outOfBoundary = false;
		return true;
	}
	size_t getRowIndex(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		return cursor;
	}
	size_t getRowNumber(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		return rowNum;
	}
	size_t getColumnNumber(string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		return cast(size_t)fileldNum;
	}
	string getString(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
        Oid type = PQftype(result, cast(int)columnIndex);
		if(type != BPCHAROID && type != VARCHAROID) {
			throw new PostgreSQLException(TYPE_EXCEPTION.STRING, file, line);
		}
		return getColumn!string(columnIndex, file, line);
	}
	string getString(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getString(index, file, line);
	}

	bool getBool(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		string val = getColumn!string(columnIndex, file, line);
		switch(val) {
		case "true": 
			return true;
		case "false": 
			return false;
		default:
			throw new PostgreSQLException(TYPE_EXCEPTION.BOOL, file, line);
		}
	}
	bool getBool(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getBool(index, file, line);
	}

	byte getByte(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		throw new PostgreSQLException(TYPE_EXCEPTION.BYTE, file, line);
	}
	byte getByte(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		throw new PostgreSQLException(TYPE_EXCEPTION.BYTE, file, line);
	}

	short getShort(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(PQftype(result, cast(int)columnIndex) != INT2OID) {
			throw new PostgreSQLException(TYPE_EXCEPTION.SHORT, file, line);
		}
		return getColumn!short(columnIndex, file, line);
	}
	short getShort(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getShort(index, file, line);
	}

	int getInt(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(PQftype(result, cast(int)columnIndex) != INT4OID) {
			throw new PostgreSQLException(TYPE_EXCEPTION.INT, file, line);
		}
		return getColumn!int(columnIndex, file, line);
	}
	int getInt(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getInt(index, file, line);
	}

	long getLong(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(PQftype(result, cast(int)columnIndex) != INT8OID) {
			throw new PostgreSQLException(TYPE_EXCEPTION.LONG, file, line);
		}
		return getColumn!long(columnIndex, file, line);
	}
	long getLong(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getInt(index, file, line);
	}

	float getFloat(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(PQftype(result, cast(int)columnIndex) != FLOAT4OID) {
			throw new PostgreSQLException(TYPE_EXCEPTION.FLOAT, file, line);
		}
		return getColumn!float(columnIndex, file, line);
	}
	float getFloat(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getFloat(index, file, line);
	}

	double getDouble(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(PQftype(result, cast(int)columnIndex) != FLOAT8OID) {
			throw new PostgreSQLException(TYPE_EXCEPTION.DOUBLE, file, line);
		}
		return getColumn!double(columnIndex, file, line);
	}
	double getDouble(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new PostgreSQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getDouble(index, file, line);
	}
}

enum CLOSE_EXCEPTION : string {
	ALCLOSE = "resultset already closed"
}
enum TYPE_EXCEPTION : string {
	STRING = "column can't cast to string",
	BOOL   = "column can't cast to bool",
	BYTE   = "column can't cast to byte",
	SHORT  = "column can't cast to short",
	INT    = "column can't cast to int",
	LONG   = "column can't cast to long",
	FLOAT  = "column can't cast to float",
	DOUBLE = "column can't cast to double"
}

enum COLUMN_EXCEPTION : string {
	LABEL = "no such column label",
	INDEX = "no such column index"
}
