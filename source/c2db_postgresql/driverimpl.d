module c2db_postgresql.driverimpl;

import c2db.driver;
import c2db.connection;
import c2db.url;
import c2db_postgresql.exception;
import c2db_postgresql.connectionimpl;
import c2db_postgresql.proto;

import std.string;
import std.conv;

class DriverImpl : Driver {
public:
	string getVersion() {
		int vers = PQlibVersion();
        int major = vers / 10000;
        vers %= 10000;
        int release = vers / 100;
        vers %= 100;
		return to!(string)(major) ~ '.' ~ to!(string)(release) ~ '.'
                    ~ to!(string)(vers);
	}
	uint getMajorVersion() {
		int vers = PQlibVersion();
		return cast(uint)(vers / 10000);
	}
	uint getMinorVersion() {
		int vers = PQlibVersion();
		vers %= 10000;
		return cast(uint)(vers / 100);
	}
	Connection connect(string url, string file, size_t line) {
		Url c2dbUrl = Url.parse(url);
		cstring host = toStringz(c2dbUrl.host);
		cstring port = toStringz(c2dbUrl.port);
		cstring user = toStringz(c2dbUrl.user);
		cstring password = toStringz(c2dbUrl.password);
		cstring dbname = toStringz(c2dbUrl.path) + 1;
		PGconn* conn = PQsetdbLogin(host, port, null, null, dbname, user, password);
		if(conn == null) {
			throw new PostgreSQLException("postgresql driver init failed", file, line);
		}
        if(PQstatus(conn) != ConnStatusType.CONNECTION_OK) {
			string msg = fromStringz(PQerrorMessage(conn)).idup;
			throw new PostgreSQLException(msg, file, line);
		}
		return new ConnectionImpl(conn);
	}
}
