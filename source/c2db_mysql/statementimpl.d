module c2db_mysql.statementimpl;

import c2db.statement;
import c2db.resultset;
import c2db.connection;
import c2db_mysql.resultsetimpl;
import c2db_mysql.exception;
import c2db_mysql.connectionimpl;
import c2db_mysql.proto;

import std.string;
import std.conv;
import core.stdc.config;

class StatementImpl : Statement {
private:
	bool enableEscape_;
	bool isClosed_;

	uint createdResultSet;

	Connection pconn;
	MYSQL* conn;

	string batch;

	string escapeSql(string sql) {
		// it is safest way to contain the encoded sql
		char[] str = new char[2 * sql.length];
		scope(exit) {
			destroy(str);
		}
		size_t len = mysql_real_escape_string(
				conn, str.ptr, sql.ptr, to!c_ulong(sql.length));
		return str[0 .. len].idup;
	}
public:
	this(Connection pconn, MYSQL* conn) {
		isClosed_ = false;
		enableEscape_ = false;
		createdResultSet = 0;
		
		this.pconn = pconn;
		this.conn = conn;
	}
	void close(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(createdResultSet != 0) {
			throw new MySQLException(CLOSE_EXCEPTION.UNCLOSE, file, line);
		}
		isClosed_ = true;
		ConnectionImpl connImpl = cast(ConnectionImpl)pconn;
		connImpl.releaseStatement();
	}
	Connection getConnection(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		return pconn;
	}
	bool isClosed() {
		return isClosed_;
	}
	void setEscapeProcessing(bool enable, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		enableEscape_ = enable;
	}

	bool execute(string sql, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(enableEscape_) {
			sql = escapeSql(sql);
		}
		c_ulong length = sql.length;
		int ret = mysql_real_query(conn, sql.ptr, length);
		if(ret != 0) {
			string msg = fromStringz(mysql_error(conn)).idup;
			throw new MySQLException(msg, file, line);
		}
		return mysql_field_count(conn) != 0;
	}

	ResultSet executeQuery(string sql, string file, size_t line) {
		bool ret = execute(sql, file, line);
		if(ret != true) {
			throw new MySQLException(EXECUTE_EXCEPTION.QUERY, file, line);
		}
		return getResultSet(file, line);
	}
	ulong executeUpdate(string sql, string file, size_t line) {
		bool ret = execute(sql, file, line);
		if(ret == true) {
			throw new MySQLException(EXECUTE_EXCEPTION.UPDATE, file, line);
		}
		return mysql_affected_rows(conn);
	}

	void addBatch(string sql, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(batch == null) {
			batch = sql;
			return;
		}
		batch ~= ";";
		batch ~= sql;
	}
	void clearBatch(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		batch = null;
	}
	void executeBatch(string file, size_t line) {
		execute(batch, file, line);
	}

	bool getMoreResults(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(mysql_more_results(conn) == 0) {
			return false;
		}
		mysql_next_result(conn);
		return true;
	}
	ResultSet getResultSet(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		MYSQL_RES* result = mysql_store_result(conn);
		if(result == null) {
			if(mysql_field_count(conn) == 0) {
				throw new MySQLException(RESULT_EXCEPTION.UPDATE, file, line);
			} 
			string msg = fromStringz(mysql_error(conn)).idup;
			throw new MySQLException(msg, file, line);
		}
		++createdResultSet;
		return new ResultSetImpl(this, result);
	}
	// not public for DDBC
	void releaseResultSet() {
		--createdResultSet;
	}
}

enum RESULT_EXCEPTION : string {
	UPDATE = "update operation doesn't have resultset"
}
enum CLOSE_EXCEPTION : string {
	UNCLOSE = "statement has resultset unclosed",
	ALCLOSE = "statement already closed"
}
enum EXECUTE_EXCEPTION : string {
	QUERY = "operation doesn't belong to query",
	UPDATE = "operation doesn't belong to update"
}
