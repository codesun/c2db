module c2db_mysql.exception;

import c2db.exception;

class MySQLException : SQLException {
public:
	this(string msg, string file, size_t line) {
		super(msg, file, line);
	}
}
