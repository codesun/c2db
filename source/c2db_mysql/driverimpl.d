module c2db_mysql.driverimpl;

import c2db.driver;
import c2db.connection;
import c2db.url;
import c2db_mysql.exception;
import c2db_mysql.connectionimpl;
import c2db_mysql.proto;

import std.string;
import std.conv;

class DriverImpl : Driver {
public:
	string getVersion() {
		cstring vers = mysql_get_client_info();
		return fromStringz(vers).idup;
	}
	uint getMajorVersion() {
		ulong vers = mysql_get_client_version();
		return cast(uint)(vers / 10000);
	}
	uint getMinorVersion() {
		ulong vers = mysql_get_client_version();
		vers %= 10000;
		return cast(uint)(vers / 100);
	}
	Connection connect(string url, string file, size_t line) {
		Url c2dbUrl = Url.parse(url);
		MYSQL* driver = mysql_init(null);
		if(driver == null) {
			string msg = "mysql driver init failed";
			throw new MySQLException(msg, file, line);
		}
		cstring host = toStringz(c2dbUrl.host);
		uint    port = to!uint(c2dbUrl.port);
		cstring user = toStringz(c2dbUrl.user);
		cstring password = toStringz(c2dbUrl.password);
		cstring dbname = toStringz(c2dbUrl.path) + 1;
		MYSQL* conn = mysql_real_connect(driver, host, user,
				password, dbname, port, null, CLIENT_MULTI_STATEMENTS);
		if(conn == null) {
			string msg = fromStringz(mysql_error(driver)).idup;
			throw new MySQLException(msg, file, line);
		}
		// TODO: check here
		string sTimeout = c2dbUrl.query("timeout");
		if(sTimeout != null) {
			uint timeout = to!uint(sTimeout);
			mysql_options(conn, MysqlOption.MYSQL_OPT_CONNECT_TIMEOUT, &timeout);
		}

		return new ConnectionImpl(conn);
	}
}
