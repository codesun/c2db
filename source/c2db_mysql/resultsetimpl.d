module c2db_mysql.resultsetimpl;

import c2db.resultset;
import c2db.statement;
import c2db_mysql.exception;
import c2db_mysql.proto;
import c2db_mysql.statementimpl;

import std.string;
import std.conv;

class ResultSetImpl : ResultSet {
private:
	bool isClosed_;

	bool outOfBoundary;

	Statement stat;
	MYSQL_RES* result;

	MYSQL_ROW[] rows;
	size_t cursor;

	MYSQL_FIELD* fields;
	uint[string] indexMap;

	void prepareFields() {
		uint count = mysql_num_fields(result);
		fields = mysql_fetch_fields(result);
		for(int i = 0; i < count; ++i) {
			indexMap[fromStringz(fields[i].org_name)] = i;
		}
	}
	void prepareRows() {
		// warning: may cause bug when rows count > uint.max inside x86
		size_t numRows = cast(size_t)mysql_num_rows(result);
		rows = new MYSQL_ROW[numRows];
		for(int i = 0; i < numRows; ++i) {
			rows[i] = mysql_fetch_row(result);
		}
	}

	T getColumn(T)(size_t columnIndex, string file, size_t line) {
		if(columnIndex >= mysql_num_fields(result)) {
			throw new MySQLException(COLUMN_EXCEPTION.INDEX, file, line);
		}
		string val = fromStringz(rows[cursor][columnIndex]).idup;
		return to!T(val);
	}
	uint getColumnIndex(string label, string file, size_t line) {
		if(label !in indexMap) {
			throw new MySQLException(COLUMN_EXCEPTION.LABEL, file, line);
		}
		return indexMap[label];
	}
public:
	this(Statement stat, MYSQL_RES* result) {
		isClosed_ = false;
		this.stat = stat;
		this.result = result;
		prepareFields();
		prepareRows();
		outOfBoundary = true;
	}
	Statement getStatement(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		return stat;
	}
	void close(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		isClosed_ = true;
		indexMap = null;
		destroy(rows);
		rows = null;
		mysql_free_result(result);
		StatementImpl state = cast(StatementImpl)stat;
		state.releaseResultSet();
	}

	bool isClosed() {
		return isClosed_;
	}

	bool isFirst(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		return !outOfBoundary && cursor == 0;
	}

	bool isLast(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		return !outOfBoundary && cursor == rows.length - 1;
	}

	bool next(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		if(!outOfBoundary) {
			if(cursor == rows.length - 1) {
				outOfBoundary = true;
				return false;
			}
			++cursor;
			return true;
		} else {
			if(cursor == 0) {
				outOfBoundary = false;
				return true;
			}
			return false;
		}
	}
	bool previous(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		if(!outOfBoundary) {
			if(cursor == 0) {
				outOfBoundary = true;
				return false;
			}
			--cursor;
			return true;
		} else {
			if(cursor == rows.length - 1) {
				outOfBoundary = false;
				return true;
			}
			return false;
		}
	}
	bool absolute(size_t rowIndex, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		if(rowIndex < 0 || rowIndex > rows.length - 1) {
			return false;
		}
		cursor = rowIndex;
		outOfBoundary = false;
		return true;
	}
	bool first(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		if(rows.length == 0) {
			return false;
		}
		cursor = 0;
		outOfBoundary = false;
		return true;
	}
	bool last(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		if(rows.length == 0) {
			return false;
		}
		cursor = rows.length - 1;
		outOfBoundary = false;
		return true;
	}
	size_t getRowIndex(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		return cursor;
	}
	size_t getRowNumber(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		return rows.length;
	}
	size_t getColumnNumber(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}

		return mysql_num_fields(result);
	}
	string getString(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(fields[columnIndex].type != MYSQL_TYPE_STRING && 
				fields[columnIndex].type != MYSQL_TYPE_VAR_STRING) {
			throw new MySQLException(TYPE_EXCEPTION.STRING, file, line);
		}
		return getColumn!string(columnIndex, file, line);
	}
	string getString(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getString(index, file, line);
	}

	bool getBool(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		string val = getColumn!string(columnIndex, file, line);
		switch(val) {
		case "true": 
			return true;
		case "false": 
			return false;
		default:
			throw new MySQLException(TYPE_EXCEPTION.BOOL, file, line);
		}
	}
	bool getBool(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getBool(index, file, line);
	}

	byte getByte(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(fields[columnIndex].type != MYSQL_TYPE_TINY) {
			throw new MySQLException(TYPE_EXCEPTION.BYTE, file, line);
		}
		return getColumn!byte(columnIndex, file, line);
	}
	byte getByte(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getByte(index, file, line);
	}

	short getShort(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(fields[columnIndex].type != MYSQL_TYPE_SHORT) {
			throw new MySQLException(TYPE_EXCEPTION.SHORT, file, line);
		}
		return getColumn!short(columnIndex, file, line);
	}
	short getShort(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getShort(index, file, line);
	}

	int getInt(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(fields[columnIndex].type != MYSQL_TYPE_LONG) {
			throw new MySQLException(TYPE_EXCEPTION.INT, file, line);
		}
		return getColumn!int(columnIndex, file, line);
	}
	int getInt(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getInt(index, file, line);
	}

	long getLong(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(fields[columnIndex].type != MYSQL_TYPE_LONGLONG) {
			throw new MySQLException(TYPE_EXCEPTION.LONG, file, line);
		}
		return getColumn!long(columnIndex, file, line);
	}
	long getLong(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getInt(index, file, line);
	}

	float getFloat(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(fields[columnIndex].type != MYSQL_TYPE_FLOAT) {
			throw new MySQLException(TYPE_EXCEPTION.FLOAT, file, line);
		}
		return getColumn!float(columnIndex, file, line);
	}
	float getFloat(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getFloat(index, file, line);
	}

	double getDouble(size_t columnIndex, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(fields[columnIndex].type != MYSQL_TYPE_DOUBLE) {
			throw new MySQLException(TYPE_EXCEPTION.DOUBLE, file, line);
		}
		return getColumn!double(columnIndex, file, line);
	}
	double getDouble(string columnLabel, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		uint index = getColumnIndex(columnLabel, file, line);
		return getDouble(index, file, line);
	}
}

enum CLOSE_EXCEPTION : string {
	ALCLOSE = "resultset already closed"
}
enum TYPE_EXCEPTION : string {
	STRING = "column can't cast to string",
	BOOL   = "column can't cast to bool",
	BYTE   = "column can't cast to byte",
	SHORT  = "column can't cast to short",
	INT    = "column can't cast to int",
	LONG   = "column can't cast to long",
	FLOAT  = "column can't cast to float",
	DOUBLE = "column can't cast to double"
}

enum COLUMN_EXCEPTION : string {
	LABEL = "no such column label",
	INDEX = "no such column index"
}
