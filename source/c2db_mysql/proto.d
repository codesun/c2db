module c2db_mysql.proto;

import std.string;
import core.stdc.config;

/**
 * MySQL raw API for DDBC, obtained from mysql.h
 * only MySQL(version > 5.1) is supported
 *
 * @author: CodeSun
 */
extern(System) {
/* ========================================= */
/* | Libmysqlclient Enumeration Definition | */
/* ========================================= */

/* For flags inside MYSQL_FIELD */
enum : uint {
	NOT_NULL_FLAG = 1,                 /* Field can't be NULL */
	PRI_KEY_FLAG = 2,                  /* Field is part of a primary key */
	UNIQUE_KEY_FLAG = 4,               /* Field is part of a unique key */
	MULTIPLE_KEY_FLAG = 8,             /* Field is part of a key */
	BLOB_FLAG = 16,                    /* Field is a blog */
	UNSIGNED_FLAG = 32,                /* Field is unsigned */
	ZEROFILL_FLAG = 64,                /* Field is zerofill */
	BINARY_FLAG = 128,                 /* Field is binary */
/* The following are only sent to new clients */
	ENUM_FLAG	= 256,	               /* field is an enum */
	AUTO_INCREMENT_FLAG = 512,         /* field is a autoincrement field */
	TIMESTAMP_FLAG = 1024,             /* Field is a timestamp */
	SET_FLAG = 2048,                   /* field is a set */
	NO_DEFAULT_VALUE_FLAG = 4096,	   /* Field doesn't have default value */
	ON_UPDATE_NOW_FLAG = 8192,         /* Field is set to NOW on UPDATE */
	NUM_FLAG = 32768,	               /* Field is num (for clients) */
	PART_KEY_FLAG	= 16384,           /* Intern: Part of some key */
	GROUP_FLAG = 32768,                /* Intern: Group field */
	UNIQUE_FLAG = 65536,               /* Intern: Used by sql_yacc */
	BINCMP_FLAG = 131072,	           /* Intern: Used by sql_yacc */
	GET_FIXED_FIELDS_FLAG = 1 << 18,   /* Used to get fields in item tree */
	FIELD_IN_PART_FUNC_FLAG = 1 << 19  /* Field part of partition func */
}
	
/* For client_flag inside mysql_real_connect */
enum : c_ulong {
	CLIENT_LONG_PASSWORD = 1,	        /* new more secure passwords */
	CLIENT_FOUND_ROWS = 2,	            /* Found instead of affected rows */
	CLIENT_LONG_FLAG = 4,	            /* Get all column flags */
	CLIENT_CONNECT_WITH_DB = 8,	        /* One can specify db on connect */
	CLIENT_NO_SCHEMA = 16,	            /* Don't allow database.table.column */
	CLIENT_COMPRESS = 32,	            /* Can use compression protocol */
	CLIENT_ODBC = 64,	                /* Odbc client */
	CLIENT_LOCAL_FILES	= 128,          /* Can use LOAD DATA LOCAL */
	CLIENT_IGNORE_SPACE = 256,	        /* Ignore spaces before '(' */
	CLIENT_PROTOCOL_41	= 512,	        /* New 4.1 protocol */
	CLIENT_INTERACTIVE = 1024,	        /* This is an interactive client */
	CLIENT_SSL = 2048,	                /* Switch to SSL after handshake */
	CLIENT_IGNORE_SIGPIPE = 4096,       /* IGNORE sigpipes */
	CLIENT_TRANSACTIONS = 8192,	        /* Client knows about transactions */
	CLIENT_RESERVED = 16384,            /* Old flag for 4.1 protocol  */
	CLIENT_SECURE_CONNECTION = 32768,   /* New 4.1 authentication */
	CLIENT_MULTI_STATEMENTS = 1 << 16,  /* Enable/disable multi-stmt support */
	CLIENT_MULTI_RESULTS = 1 << 17,     /* Enable/disable multi-results */
	CLIENT_PS_MULTI_RESULTS = 1 << 18,  /* Multi-results in PS-protocol */
	CLIENT_PLUGIN_AUTH = 1 << 19,       /* Client supports plugin authentication */
	CLIENT_CONNECT_ATTRS = 1 << 20,     /* Client supports connection attributes */
/* Enable authentication response packet to be larger than 255 bytes. */
	CLIENT_PLUGIN_AUTH_LENENC_CLIENT_DATA = 1 << 21,
/* Don't close the connection for a connection with expired password. */
	CLIENT_CAN_HANDLE_EXPIRED_PASSWORDS = 1 << 22,
	CLIENT_PROGRESS = 1 << 29,          /* Client support progress indicator */
	CLIENT_SSL_VERIFY_SERVER_CERT = 1 << 30,
/*
  It used to be that if mysql_real_connect() failed, it would delete any
  options set by the client, unless the CLIENT_REMEMBER_OPTIONS flag was
  given.
  That behaviour does not appear very useful, and it seems unlikely that
  any applications would actually depend on this. So from MariaDB 5.5 we
  always preserve any options set in case of failed connect, and this
  option is effectively always set.
*/
	CLIENT_REMEMBER_OPTIONS = 1 << 31
}

/* For type inside MYSQL_FIELD */
enum : uint {
    MYSQL_TYPE_DECIMAL,
	MYSQL_TYPE_TINY,
    MYSQL_TYPE_SHORT,
	MYSQL_TYPE_LONG,
    MYSQL_TYPE_FLOAT,
	MYSQL_TYPE_DOUBLE,
    MYSQL_TYPE_NULL,
	MYSQL_TYPE_TIMESTAMP,
    MYSQL_TYPE_LONGLONG,
	MYSQL_TYPE_INT24,
    MYSQL_TYPE_DATE,
	MYSQL_TYPE_TIME,
    MYSQL_TYPE_DATETIME,
	MYSQL_TYPE_YEAR,
    MYSQL_TYPE_NEWDATE,
	MYSQL_TYPE_VARCHAR,
    MYSQL_TYPE_BIT,
       
        // mysql-5.6 compatibility temporal types.
        // They're only used internally for reading RBR
        // mysql-5.6 binary log events and mysql-5.6 frm files.
        // They're never sent to the client.
        
        MYSQL_TYPE_TIMESTAMP2,
        MYSQL_TYPE_DATETIME2,
        MYSQL_TYPE_TIME2,
        
        MYSQL_TYPE_NEWDECIMAL=246,

    MYSQL_TYPE_ENUM=247,
    MYSQL_TYPE_SET=248,
    MYSQL_TYPE_TINY_BLOB=249,
    MYSQL_TYPE_MEDIUM_BLOB=250,
    MYSQL_TYPE_LONG_BLOB=251,
    MYSQL_TYPE_BLOB=252,
    MYSQL_TYPE_VAR_STRING=253,
    MYSQL_TYPE_STRING=254,
    MYSQL_TYPE_GEOMETRY=255
}
/* For mysql_option */
enum MysqlOption {
  MYSQL_OPT_CONNECT_TIMEOUT, MYSQL_OPT_COMPRESS, MYSQL_OPT_NAMED_PIPE,
  MYSQL_INIT_COMMAND, MYSQL_READ_DEFAULT_FILE, MYSQL_READ_DEFAULT_GROUP,
  MYSQL_SET_CHARSET_DIR, MYSQL_SET_CHARSET_NAME, MYSQL_OPT_LOCAL_INFILE,
  MYSQL_OPT_PROTOCOL, MYSQL_SHARED_MEMORY_BASE_NAME, MYSQL_OPT_READ_TIMEOUT,
  MYSQL_OPT_WRITE_TIMEOUT, MYSQL_OPT_USE_RESULT,
  MYSQL_OPT_USE_REMOTE_CONNECTION, MYSQL_OPT_USE_EMBEDDED_CONNECTION,
  MYSQL_OPT_GUESS_CONNECTION, MYSQL_SET_CLIENT_IP, MYSQL_SECURE_AUTH,
  MYSQL_REPORT_DATA_TRUNCATION, MYSQL_OPT_RECONNECT,
  MYSQL_OPT_SSL_VERIFY_SERVER_CERT, MYSQL_PLUGIN_DIR, MYSQL_DEFAULT_AUTH,
  MYSQL_OPT_BIND,
  MYSQL_OPT_SSL_KEY, MYSQL_OPT_SSL_CERT, 
  MYSQL_OPT_SSL_CA, MYSQL_OPT_SSL_CAPATH, MYSQL_OPT_SSL_CIPHER,
  MYSQL_OPT_SSL_CRL, MYSQL_OPT_SSL_CRLPATH,
  MYSQL_OPT_CONNECT_ATTR_RESET, MYSQL_OPT_CONNECT_ATTR_ADD,
  MYSQL_OPT_CONNECT_ATTR_DELETE,
  MYSQL_SERVER_PUBLIC_KEY,
  MYSQL_ENABLE_CLEARTEXT_PLUGIN,
  MYSQL_OPT_CAN_HANDLE_EXPIRED_PASSWORDS,

  /* MariaDB options */
  MYSQL_PROGRESS_CALLBACK=5999,
  MYSQL_OPT_NONBLOCK,
  MYSQL_OPT_USE_THREAD_SPECIFIC_MEMORY
}

/* ===================================== */
/* | Libmysqlclient Typedef Definition | */
/* ===================================== */
	alias cstring = const(char)*;
	alias MYSQL_ROW = cstring*;
	alias MYSQL_ROW_OFFSET = MYSQL_ROWS*;
	alias MYSQL_FIELD_OFFSET = uint;
	alias my_bool = ubyte;

/* ===================== */
/* | Struct Definition | */
/* ===================== */
	struct MYSQL;
	struct MYSQL_RES;

	struct MYSQL_FIELD {
		cstring name;            /* Name of column */
		cstring org_name;        /* Original column name, if an alias */ 
		cstring table;           /* Table of column if column was a field */
		cstring org_table;       /* Org table name, if table was an alias */
		cstring db;              /* Database for table */
		cstring catalog;	     /* Catalog for table */
		cstring def;             /* Default value (set by mysql_list_fields) */

		c_ulong length;          /* Width of column (create length) */
		c_ulong max_length;      /* Max width for selected set */
		uint name_length;
		uint org_name_length;
		uint table_length;
		uint org_table_length;
		uint db_length;
		uint catalog_length;
		uint def_length;
		uint flags;              /* Div flags */
		uint decimals;           /* Number of decimals in field */
		uint charsetnr;          /* Character set */
		uint type;               /* Type of field */

		void* extension;
	}
	struct MYSQL_ROWS {
		MYSQL_ROWS* next;
		MYSQL_ROW data;
		c_ulong length;
	}

/* ====================================== */
/* | Libmysqlclient Function Definition | */
/* ====================================== */
	MYSQL* mysql_init(MYSQL* mysql);
	void   mysql_close(MYSQL* mysql);
	int    mysql_ping(MYSQL* mysql);
	int    mysql_options(MYSQL* mysql, MysqlOption option, const(void)* arg);

	MYSQL* mysql_real_connect(
			MYSQL*  mysql,
			cstring host,
			cstring user,
			cstring password,
			cstring db, 
			uint    port,
			cstring unix_socket,
			c_ulong client_flag
	);

	cstring mysql_sqlstate(MYSQL* mysql);
	c_ulong mysql_real_escape_string(MYSQL* mysql, char* to, cstring from, c_ulong length);

	/* For driver version */
	c_ulong mysql_get_client_version();
	cstring mysql_get_client_info();

	/* For error process */
	uint    mysql_errno(MYSQL* mysql);
	cstring mysql_error(MYSQL* mysql);

	/* For transaction */
	my_bool mysql_autocommit(MYSQL* mysql, my_bool mode);
	my_bool mysql_commit(MYSQL* mysql);
	my_bool mysql_rollback(MYSQL* mysql);

	int mysql_query(MYSQL* mysql, cstring stmt_str);
	int mysql_real_query(MYSQL* mysql, cstring stmt_str, c_ulong length);
	ulong mysql_affected_rows(MYSQL*);

	/* For mysql_row */
	ulong mysql_num_rows(MYSQL_RES* result);
	MYSQL_ROW    mysql_fetch_row(MYSQL_RES* result);
	c_ulong*     mysql_fetch_lengths(MYSQL_RES* result);
	MYSQL_ROW_OFFSET mysql_row_tell(MYSQL_RES* result);
	MYSQL_ROW_OFFSET mysql_row_seek(MYSQL_RES* result, MYSQL_ROW_OFFSET offset);
	
	/* For mysql_field */
	uint         mysql_field_count(MYSQL* mysql);
	MYSQL_FIELD* mysql_fetch_field(MYSQL_RES* result);
	MYSQL_FIELD* mysql_fetch_fields(MYSQL_RES* result);
	MYSQL_FIELD* mysql_fetch_field_direct(MYSQL_RES* result, uint fieldnr);
	uint         mysql_num_fields(MYSQL_RES* result);
	MYSQL_FIELD_OFFSET mysql_field_tell(MYSQL_RES* result);
	MYSQL_FIELD_OFFSET mysql_field_seek(MYSQL_RES* result, MYSQL_FIELD_OFFSET offset);

	//ulong mysql_insert_id(MYSQL*);

	/* For mysql_result */
	MYSQL_RES* mysql_store_result(MYSQL* mysql);
	void       mysql_free_result(MYSQL_RES* result);
	my_bool    mysql_more_results(MYSQL* mysql);
	int        mysql_next_result(MYSQL* mysql);
}
