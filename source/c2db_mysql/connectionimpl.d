module c2db_mysql.connectionimpl;

import c2db.connection;
import c2db.statement;
import c2db_mysql.exception;
import c2db_mysql.proto;
import c2db_mysql.statementimpl;

import std.string;

class ConnectionImpl : Connection {
private:
	MYSQL* conn;

	uint createdStatement;

	bool isClosed_;
	bool isAutoCommit_;

public:
	this(MYSQL* conn) {
		isClosed_ = false;
		// auto commit mode by default
		isAutoCommit_ = true;
		createdStatement = 0;

		this.conn = conn;
	}
	
	bool isClosed() {
		return isClosed_;
	}

	bool getAutoCommit(string file = __FILE__, size_t line = __LINE__) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		return isAutoCommit_;
	}

	void close(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		if(createdStatement != 0) {
			throw new MySQLException(CLOSE_EXCEPTION.UNCLOSE, file, line);
		}
		isClosed_ = true;
		mysql_close(conn);
	}

	void commit(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		int ret = mysql_commit(conn);
		if(ret != 0) {
			string msg = fromStringz(mysql_error(conn)).idup;
			throw new MySQLException(msg, file, line);
		}
	}

	void rollback(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		int ret = mysql_rollback(conn);
		if(ret != 0) {
			string msg = fromStringz(mysql_error(conn)).idup;
			throw new MySQLException(msg, file, line);
		}
	}

	void setAutoCommit(bool autoCommit, string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		isAutoCommit_ = autoCommit;
		my_bool mode = 1;
		if(!autoCommit) {
			mode = 0;
		}
		int ret = mysql_autocommit(conn, mode);
		if(ret != 0) {
			string msg = fromStringz(mysql_error(conn)).idup;
			throw new MySQLException(msg, file, line);
		}
	}
	Statement createStatement(string file, size_t line) {
		if(isClosed_) {
			throw new MySQLException(CLOSE_EXCEPTION.ALCLOSE, file, line);
		}
		++createdStatement;
		return new StatementImpl(this, conn);
	}
	// not public for DDBC
	void releaseStatement() {
		--createdStatement;
	}
}

enum CLOSE_EXCEPTION : string {
	UNCLOSE = "connection has statement unclosed",
	ALCLOSE = "connection already closed"
}
