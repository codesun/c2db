module c2db.connection;

import c2db.statement;

interface Connection {
	bool isClosed();
	void close(string file = __FILE__, size_t line = __LINE__);

	void commit(string file = __FILE__, size_t line = __LINE__);
	void rollback(string file = __FILE__, size_t line = __LINE__);

	void setAutoCommit(bool autoCommit,
			string file = __FILE__, size_t line = __LINE__);
	bool getAutoCommit(string file = __FILE__, size_t line = __LINE__);

	Statement createStatement(
			string file = __FILE__, size_t line = __LINE__);
}
