module c2db.drivermanager;

import c2db.driver;
import c2db.driverloader;

/**
 * DriverManager is used to regist or deregister Drivers
 *
 * @author: CodeSun
 */
class DriverManager {
static:
	private DriverLoader[Driver] loaderMap;
	public void deregistDriver(Driver driver) {
		loaderMap[driver].close();
	}
	public Driver registDriver(string path) {
		DriverLoader loader = new DriverLoader(path);
		Driver driver = loader.getDriver();
		loaderMap[driver] = loader;
		
		return driver;
	}
}
