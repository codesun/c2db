module c2db.driver;

import c2db.connection;

interface Driver {
	uint getMajorVersion();
	uint getMinorVersion();
	string getVersion();
	Connection connect(string url,
			string file = __FILE__, size_t line = __LINE__);
}
