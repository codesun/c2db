module c2db.resultset;

import c2db.statement;

interface ResultSet {
	bool isClosed();
	void close(string file = __FILE__, size_t line = __LINE__);
	bool isFirst(string file = __FILE__, size_t line = __LINE__);
	bool isLast(string file = __FILE__, size_t line = __LINE__);

	bool next(string file = __FILE__, size_t line = __LINE__);
	bool previous(string file = __FILE__, size_t line = __LINE__);
	bool absolute(size_t rowIndex,
			string file = __FILE__, size_t line = __LINE__);
	bool first(string file = __FILE__, size_t line = __LINE__);
	bool last(string file = __FILE__, size_t line = __LINE__);

	size_t getRowIndex(string file = __FILE__, size_t line = __LINE__);
	size_t getRowNumber(string file = __FILE__, size_t line = __LINE__);

	size_t getColumnNumber(string file = __FILE__, size_t line = __LINE__);

	string getString(size_t columnIndex,
			string file = __FILE__, size_t line = __LINE__);
	string getString(string columnLabel,
			string file = __FILE__, size_t line = __LINE__);

	bool getBool(size_t columnIndex,
			string file = __FILE__, size_t line = __LINE__);
	bool getBool(string columnLabel,
			string file = __FILE__, size_t line = __LINE__);

	byte getByte(size_t columnIndex,
			string file = __FILE__, size_t line = __LINE__);
	byte getByte(string columnLabel,
			string file = __FILE__, size_t line = __LINE__);

	short getShort(size_t columnIndex,
			string file = __FILE__, size_t line = __LINE__);
	short getShort(string columnLabel,
			string file = __FILE__, size_t line = __LINE__);

	int getInt(size_t columnIndex,
			string file = __FILE__, size_t line = __LINE__);
	int getInt(string columnLabel,
			string file = __FILE__, size_t line = __LINE__);

	long getLong(size_t columnIndex,
			string file = __FILE__, size_t line = __LINE__);
	long getLong(string columnLabel,
			string file = __FILE__, size_t line = __LINE__);

	float getFloat(size_t columnIndex,
			string file = __FILE__, size_t line = __LINE__);
	float getFloat(string columnLabel,
			string file = __FILE__, size_t line = __LINE__);

	double getDouble(size_t columnIndex,
			string file = __FILE__, size_t line = __LINE__);
	double getDouble(string columnLabel,
			string file = __FILE__, size_t line = __LINE__);

	Statement getStatement(string file = __FILE__, size_t line = __LINE__);
}
