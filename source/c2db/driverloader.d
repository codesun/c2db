module c2db.driverloader;

import core.runtime;
version(Posix) {
	import core.sys.posix.dlfcn;
}

version(Windows) {
	import core.sys.windows.windows;
	import core.sys.windows.dll;
}

import std.string;
import std.exception;

import c2db.driver;

alias retFun = Driver function();
enum string SYMBOL = "init";

/**
 * DriverLoader is used to load specific Driver of DB
 *
 * @author: CodeSun
 */
// TODO: replace the embedded error msg with Windows error function
class DriverLoader {
	private void* dll;

	this(string path) {
		dll = Runtime.loadLibrary(path);
		version(Posix) {
			enforce(dll != null, fromStringz(dlerror()));
		}
		version(Windows) {
			enforce(dll != null, "error loading Driver " ~ path);
		}
	}
	Driver getDriver() {
		version(Posix) {
			// clear invalid error msg
			dlerror();
			void* res = dlsym(dll, SYMBOL);
			enforce(res != null, fromStringz(dlerror()));
		}
		version(Windows) {
			void* res = GetProcAddress(dll, SYMBOL);
			enforce(res != null, "error loading symbol init()");
		}
		Driver obj = (cast(retFun)res)();
		return obj;
	}
	void close() {
		bool ret = Runtime.unloadLibrary(dll);
		version(Posix) {
			enforce(ret == true, fromStringz(dlerror()));
		}
		version(Windows) {
			enforce(ret == true, "error unloading Driver");
		}
	}
}
