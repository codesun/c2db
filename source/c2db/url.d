module c2db.url;

import std.string;
import std.conv;

import std.exception;

/**
 * Url is used to parse url string conforming to RFC3986(partly)
 * [RFC3986] http://www.ietf.org/rfc/rfc3986.txt 
 *
 * @version: 0.8
 * @author: CodeSun
 */

/** 
 * <scheme>:
 *         //[<user>:<password>@]<host>[:<port>]
 *         [/<path>]
 *         [?<query>]
 *         [#<frag>]
 */
class Url {
private:
	string m_Url;
	string m_Scheme;
	string m_User;
	string m_Password;
	string m_Host;
	string m_Port;
	string m_Path;
	string m_Frag;
	string[string] m_Query;

	this(string url) {
		m_Url = url;
		parseUrl();
	}
	/* [SCHEME] = ALPHA *( ALPHA / DIGIT / "+" / "-" / "." ) */
	void parseScheme(ref size_t index) {
		size_t len = m_Url.length;
		if(!isAlpha(m_Url[index++])) {
			throw new UrlException("invalid scheme");
		}
		while(index < len) {
			switch(m_Url[index]) {
				/* APPHA */
				case 'a': .. case 'z':
				case 'A': .. case 'Z':
				/* DIGIT */
				case '0': .. case '9':
				case '+', '-', '.':
					++index;
					break;
				case ':':
					goto finish;
				default:
					throw new UrlException("invalid scheme");
			}
		}
		throw new UrlException("invalid scheme");
	finish:
		m_Scheme = m_Url[0 .. index];
		/* skip ':' */
		++index;
	}
	void parseAuthority(ref size_t index) {
		size_t len = m_Url.length;
		if(index + 2 >= len || m_Url[index .. index + 2] != "//") {
			throw new UrlException("invalid authority");
		}
		/* skip "//" */
		index += 2;
		size_t begin = index;
		/* user => temp || host => temp */
		string temp;
		while(index < len) {
			switch(m_Url[index]) {
				/* ALPHA */
				case 'a': .. case 'z':
				case 'A': .. case 'Z':
				/* DIGIT */
				case '0': .. case '9':
				case '!', '$', '&', '\'', '(', ')', '*', '+', ',',
					 ';', '=', '-', '.', '_', '~', '%':
					++index;
					break;
				case ':':
					/* user or host MUST not be empty */
					if(begin == index) {
						throw new UrlException("invalid authority");
					}
					temp = m_Url[begin .. index];
					/* skip ':' */
					begin = ++index;
					break;
				case '@':
					m_User = temp;
					/* password CAN be empty */
					m_Password = m_Url[begin .. index];
					temp = null;
					/* skip '@' */
					begin = ++index;
					break;
				case '/', '?', '#':
					goto finish;
				default:
					throw new UrlException("invalid authority");
			}
		}
	finish:
		/* explicit port or host MUST not be empty */
		if(begin == index) {
			throw new UrlException("invalid authority");
		}
		if(temp == null) {
			m_Host = m_Url[begin .. index];
		} else {
			m_Host = temp;
			m_Port = m_Url[begin .. index];
		}
	}
	void parsePath(ref size_t index) {
		size_t len = m_Url.length;
		if(index >= len) {
			return;
		}
		size_t begin = index;
		while(index < len) {
			switch(m_Url[index]) {
				/* ALPHA */
				case 'a': .. case 'z':
				case 'A': .. case 'Z':
				/* DIGIT */
				case '0': .. case '9':
				case '/', '%', '-', '.', '_', '~', ':', '@', '!', '$',
					 '&', '\'', '(', ')', '*', '+', ',', ';', '=':
					++index;
					break;
				case '?', '#':
					goto finish;
				default:
					throw new UrlException("invalid path");
			}
		}
	finish:
		m_Path = m_Url[begin .. index];
	}
	void parseQuery(ref size_t index) {
		size_t len = m_Url.length;
		if(index + 1 >= len) {
			return;
		}
		/* skip '?' */
		size_t begin = ++index;
		/* key => temp */
		string temp;
		while(index < len) {
			switch(m_Url[index]) {
				/* ALPHA */
				case 'a': .. case 'z':
				case 'A': .. case 'Z':
				/* DIGIT */
				case '0': .. case '9':
				case '/', '?', '%', '-', '.', '_', '~', ':', '@', '!', '$',
					 '\'', '(', ')', '*', '+', ',', ';':
					++index;
					break;
				case '&':
					if(temp == null) {
						throw new UrlException("invalid query");
					}
					/* value CAN be empty */
					m_Query[temp] = m_Url[begin .. index];
					temp = null;
					/* skip '&' */
					begin = ++index;
					break;
				case '=':
					/* key MUST not be empty */
					if(begin == index) {
						throw new UrlException("invalid query");
					}
					temp = m_Url[begin .. index];
					/* skip '=' */
					begin = ++index;
					break;
				case '#':
					goto finish;
				default:
					throw new UrlException("invalid query");
			}
		}
	finish:
		if(temp == null) {
			throw new UrlException("invalid query");
		}
		if(begin < len) {
			m_Query[temp] = m_Url[begin .. index];
		} else {
			m_Query[temp] = "";
		}

	}
	void parseFrag(ref size_t index) {
		size_t len = m_Url.length;
		if(index + 1 >= len) {
			return;
		}
		/* skip '#' */
		size_t begin = ++index;
		while(index < len) {
			switch(m_Url[index]) {
				/* ALPHA */
				case 'a': .. case 'z':
				case 'A': .. case 'Z':
				/* DIGIT */
				case '0': .. case '9':
				case '/', '?', '%', '-', '.', '_', '~', ':', '@', '!', '$',
					 '&', '\'', '(', ')', '*', '+', ',', ';', '=':
					++index;
					break;
				default:
					throw new UrlException("invalid fragment");
			}
		}
		if(begin >= len) {
			throw new UrlException("invalid fragment");
		}
		m_Frag = m_Url[begin .. index];
	}
	void parseUrl() {
		size_t index = 0;
		parseScheme(index);
		parseAuthority(index);
		parsePath(index);
		parseQuery(index);
		parseFrag(index);
	}
public:
	static Url parse(string url) {
		return new Url(url);
	}
	@property
	string scheme() {
		return m_Scheme;
	}
	@property
	string user() {
		return m_User;
	}
	@property
	string password() {
		return m_Password;
	}
	@property
	string host() {
		return m_Host;
	}
	@property
	string port() {
		return m_Port;
	}
	@property
	string path() {
		return m_Path;
	}
	@property
	string fragment() {
		return m_Frag;
	}
	string query(string key) {
		return m_Query.get(key, null);
	}
}

class UrlException : Exception {
	this(string msg) {
		super(msg);
	}
}

bool isAlpha(char ch) {
	bool res;
	switch(ch) {
		case 'a': .. case 'z':
		case 'A': .. case 'Z':
			res = true;
			break;
		default:
			res = false;
	}
	return res;
}
bool isDigit(char ch) {
	bool res;
	switch(ch) {
		case '0': .. case '9':
			res = true;
			break;
		default:
			res = false;
	}
	return res;
}
bool isHexDigit(char ch) {
	bool res;
	switch(ch) {
		case '0': .. case '9':
		case 'A': .. case 'F':
			res = true;
			break;
		default:
			res = false;
	}
	return res;
}

unittest {
	Url url = Url.parse("ddbc://codesun:root@127.0.0.1:3306/?user=root&password=&age=23#test");
	assert(url.scheme == "ddbc");
	assert(url.user == "codesun");
	assert(url.password == "root");
	assert(url.host == "127.0.0.1");
	assert(url.port == "3306");
	assert(url.path == "/");
	assert(url.query("user") == "root");
	assert(url.query("password") == "");
	assert(url.query("age") == "23");
	assert(url.fragment == "test");

	url = Url.parse("ddbc://localhost/student/codesun?user=root&password=123&age=");
	assert(url.scheme == "ddbc");
	assert(url.host == "localhost");
	assert(url.port == null);
	assert(url.path == "/student/codesun");
	assert(url.query("user") == "root");
	assert(url.query("password") == "123");
	assert(url.query("age") == "");
}
