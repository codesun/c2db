module c2db.statement;

import c2db.resultset;
import c2db.connection;

interface Statement {
	bool isClosed();
	void close(string file = __FILE__, size_t line = __LINE__);

	void setEscapeProcessing(bool enable,
			string file = __FILE__, size_t line = __LINE__);

	void addBatch(string sql, string file = __FILE__, size_t line = __LINE__);
	void clearBatch(string file = __FILE__, size_t line = __LINE__);

	void      executeBatch(string file = __FILE__, size_t line = __LINE__);
	ResultSet executeQuery(string sql,
			string file = __FILE__, size_t line = __LINE__);
	ulong     executeUpdate(string sql,
			string file = __FILE__, size_t line = __LINE__);
	bool      execute(string sql,
			string file = __FILE__, size_t line = __LINE__);

	bool      getMoreResults(string file = __FILE__, size_t line = __LINE__);
	ResultSet getResultSet(string file = __FILE__, size_t line = __LINE__);

	Connection getConnection(string file = __FILE__, size_t line = __LINE__);
}
