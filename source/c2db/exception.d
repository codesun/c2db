module c2db.exception;

class C2dbException : Exception {
	this(string msg, string file, size_t line) {
		super(msg, file, line);
	}
}

class SQLException : C2dbException {
	this(string msg, string file, size_t line) {
		super(msg, file, line);
	}
}

