#C2DB

C2DB is the abbreviation of "Connect to database", which provides toolkits and drivers for Dlang to simplify the operation of DB. With C2DB, you can load the driver dynamically, such as c2db-mysql, c2db-postgresql, and then use them with the same API.

###Version
* C2DB-0.2
* C2DB-MySQL-0.5
* C2DB-PostgreSQL-0.2

###Drivers
* c2db-mysql - testing
* c2db-postgresql - testing
* c2db-pool - pending
* c2db-orm - pending

###Notice
It is not recommanded to use it in multithread procedures directly, because c2db is not multithread-safe. However you can use c2db-pool or c2db-orm to accomplish the same task easily without considering any problem of multithread safety.

###Bugs
1. It will cause weird segment fault, if you don't catch the exceptions thrown by driver.
2. If you use c2db under windows, it will cause weird error while throwing an exception from the driver(.dll), it's the bug of druntime.

###Compiler Compatibility
* dmd - supported
* gdc - untested
* ldc - untested

###Platform Compatibility
* Windows - partly supported
* Linux   - supported
* Mac     - untested
* FreeBSD - untested
