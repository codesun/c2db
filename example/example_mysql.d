import c2db.driver;
import c2db.connection;
import c2db.statement;
import c2db.resultset;
import c2db.drivermanager;
import std.stdio;

void main() {
	Driver dd = DriverManager.registDriver("./libc2db-mysql.so");

	Connection conn = dd.connect("c2db://root:@127.0.0.1:3306/test");

	Statement st = conn.createStatement();

	ulong line;
	line = st.executeUpdate("create table test(id int, name varchar(20), primary key(id))");
	writeln("Exec Sql: ", "create table test(id int, name varchar(20), primary key(id))");
	writeln("Affect lines: ", line);

	line = st.executeUpdate("insert into test(id, name) values(1, 'test1'), (2, 'test2')");
	writeln("Exec Sql: ", "insert into test(id, name) values(1, 'test1'), (2, 'test2')");
	writeln("Affect lines: ", line);
	
	ResultSet rs = st.executeQuery("select * from test");
	while(rs.next()) {
		writeln("| ", rs.getInt("id"), " | ", rs.getString("name"), " |");
	}
	line = st.executeUpdate("delete from test where id >= 1");
	writeln("Exec Sql: ", "delete from test where id >= 1");
	writeln("Affect lines: ", line);

	line = st.executeUpdate("drop table test");
	writeln("Exec Sql: ", "drop table test");
	writeln("Affect lines: ", line);

	rs.close();
	st.close();
	conn.close();
	DriverManager.deregistDriver(dd);
}
