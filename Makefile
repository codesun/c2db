# C2DB Makefile
#
# Usage:
# 	make [platform=(posix|win)]

#=============
#| Variables |
#=============
platform = posix

#=====================
#| Compile Parameter |
#=====================
DD = dmd
INCLUDE = -Isource/
STDLIB = -defaultlib=libphobos2.so

EXAMPLE-PATH = example
TARGET-PATH = build

PATH-C2DB = source/c2db
PATH-MYSQL = source/c2db_mysql
PATH-PGSQL = source/c2db_postgresql

MYSQL-LIB = libs/mysql/libmysql.lib
PGSQL-LIB = libs/pgsql/libpq.lib

C2DB-BASE = $(PATH-C2DB)/driver.d     \
	        $(PATH-C2DB)/connection.d \
	        $(PATH-C2DB)/statement.d  \
	        $(PATH-C2DB)/exception.d  \
	        $(PATH-C2DB)/resultset.d     

C2DB = $(PATH-C2DB)/drivermanager.d \
	   $(PATH-C2DB)/driverloader.d  \
	   $(C2DB-BASE)

C2DB-MYSQL = $(PATH-C2DB)/url.d             \
	         $(PATH-MYSQL)/exception.d      \
			 $(PATH-MYSQL)/proto.d          \
			 $(PATH-MYSQL)/driverimpl.d     \
			 $(PATH-MYSQL)/connectionimpl.d \
			 $(PATH-MYSQL)/statementimpl.d  \
			 $(PATH-MYSQL)/resultsetimpl.d  \
			 $(PATH-MYSQL)/init.d

C2DB-PGSQL = $(PATH-C2DB)/url.d             \
			 $(PATH-PGSQL)/exception.d      \
			 $(PATH-PGSQL)/proto.d          \
			 $(PATH-PGSQL)/driverimpl.d     \
			 $(PATH-PGSQL)/connectionimpl.d \
			 $(PATH-PGSQL)/statementimpl.d  \
			 $(PATH-PGSQL)/resultsetimpl.d  \
			 $(PATH-PGSQL)/init.d

C2DB-MYSQL-WIN = $(C2DB-MYSQL) $(C2DB-BASE)
C2DB-PGSQL-WIN = $(C2DB-PGSQL) $(C2DB-BASE)

#=================
#| Compile Rules |
#=================
ifeq ($(platform), posix)
	drivers = libc2db-mysql.so libc2db-pgsql.so
	flags = -shared -fPIC -Isource -defaultlib=libphobos2.so
	common-flags = -defaultlib=libphobos2.so -L-ldl
endif

ifeq ($(platform), win)
	drivers = libc2db-mysql.dll libc2db-pgsql.dll
	flags = -shared
endif

all: $(drivers) examples

examples: example-mysql example-pgsql

#====================
#| Compile Examples |
#====================
example-mysql: $(EXAMPLE-PATH)/example_mysql.d $(C2DB)
	$(DD) $(common-flags) -of$(TARGET-PATH)/$@ $^

example-pgsql: $(EXAMPLE-PATH)/example_pgsql.d $(C2DB)
	$(DD) $(common-flags) -of$(TARGET-PATH)/$@ $^

#===================================
#| Compile dynamic version Drivers |
#===================================
libc2db-mysql.dll: $(C2DB-MYSQL-WIN) $(MYSQL-LIB)
	$(DD) $(flags) -of$(TARGET-PATH)/$@ $^

libc2db-pgsql.dll: $(C2DB-PGSQL-WIN) $(PGSQL-LIB)
	$(DD) $(flags) -of$(TARGET-PATH)/$@ $^

libc2db-mysql.so: $(C2DB-MYSQL)
	$(DD) $(flags) -L-lmysqlclient -of$(TARGET-PATH)/$@ $^

libc2db-pgsql.so: $(C2DB-PGSQL)
	$(DD) $(flags) -L-lpq -of$(TARGET-PATH)/$@ $^

.PHONY: clean
clean: 
	rm build/*
